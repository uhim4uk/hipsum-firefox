const contentNode = document.querySelector('.text-content');
const preloaderNode = document.querySelector('.preloader-wrapper');
/**
 * Sends request to hipsum api
 * @returns {Promise}
 */
const getHipsum = () => {
  preloaderNode.classList.add('loading');
  return fetch('http://hipsterjesus.com/api/').then(response =>
    response.json()
  );
};

/**
 * Returns random integer index between 0 and max length
 * @param {number} maxLength
 */
const getRandomIndex = maxLength => {
  return Math.floor(Math.random() * maxLength);
};

/**
 * Transforms first char to uppercase
 * @param {string} str
 */
const firstToUpper = str => str[0].toUpperCase() + str.slice(1);

/**
 * Creates string for title and subtitle
 * @param {string[]} arr Array of words
 * @returns {(strLen : number, min: number) => string}
 */
const createOneLine = arr => (strLen, min = 0) => {
  if (!arr.length) return '';

  const resultStrLength = getRandomIndex(strLen) + min;
  let i = 1;
  let result = '';
  while (i <= resultStrLength) {
    result += ` ${arr[getRandomIndex(arr.length)]}`;
    i++;
  }
  result = result.trim();

  return firstToUpper(result);
};

/**
 * Splits Api response into 3 arrays with words
 * @param {string[]} arr Array of words
 * @returns {string[]} arr
 */
const getParagraphs = arr => {
  if (!arr.length) return '';

  const thirdL = Math.floor(arr.length / 3);
  const cutThird = (start, end) =>
    firstToUpper(arr.slice(start, end).join(' '));

  return [
    cutThird(0, thirdL),
    cutThird(thirdL, thirdL * 2),
    cutThird(thirdL * 2, arr.length)
  ];
};

/**
 * Creates DOM node and adds copy to clipboard event listener to it
 * @param {string} text Element's text content
 * @returns {HTMLElement} node DOM node element
 */
const createDomNode = text => {
  const elem = `
  <div class="text-field">
    <div class="done-overlay">Coppied!</div>
    <button class="copy-button">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
      >
        <path fill="none" d="M0 0h24v24H0V0z" />
        <path
          d="M15 1H4c-1.1 0-2 .9-2 2v13c0 .55.45 1 1 1s1-.45 1-1V4c0-.55.45-1 1-1h10c.55 0 1-.45 1-1s-.45-1-1-1zm.59 4.59l4.83 4.83c.37.37.58.88.58 1.41V21c0 1.1-.9 2-2 2H7.99C6.89 23 6 22.1 6 21l.01-14c0-1.1.89-2 1.99-2h6.17c.53 0 1.04.21 1.42.59zM15 12h4.5L14 6.5V11c0 .55.45 1 1 1z"
        />
      </svg>
    </button>
    <div class="text-area">${text}</div>
  </div>
  `;

  const node = new DOMParser().parseFromString(elem, 'text/html').firstChild;
  const button = node.querySelector('.copy-button');
  const textNode = node.querySelector('.text-area');
  const doneNode = node.querySelector('.done-overlay');

  button.addEventListener('click', () => {
    const selection = window.getSelection();
    const range = document.createRange();
    selection.removeAllRanges();
    range.selectNodeContents(textNode);
    selection.addRange(range);
    document.execCommand('copy');
    selection.removeAllRanges();
    doneNode.classList.add('visible');
  });
  return node;
};

/**
 * Creates Title, Subtitle and 3 paragraphs and renders the to the DOM
 * @param {Object} data
 * @param {string} data.text Hipsum text with random words
 */
const renderData = data => {
  if (!data.text) {
    console.error('No text to show');
    return;
  }

  const wordArray = data.text.split(' ');
  const createHeading = createOneLine(wordArray);
  const fragment = document.createDocumentFragment();
  const textsArr = [
    createHeading(6),
    createHeading(12, 6),
    ...getParagraphs(wordArray)
  ];

  textsArr.map(text => fragment.appendChild(createDomNode(text)));

  contentNode.appendChild(fragment);
  preloaderNode.classList.remove('loading');
};

getHipsum().then(renderData);
