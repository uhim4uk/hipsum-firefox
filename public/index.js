const contentNode = document.querySelector('.text-content');
const preloaderNode = document.querySelector('.preloader-wrapper');
const initialArr = [
  'photo booth',
  'etsy',
  'subway',
  'tile',
  'snackwave',
  'twee',
  'franzen',
  'Iceland',
  'kickstarter',
  'sustainable',
  'chambray',
  'gastropub',
  'distillery',
  'yuccie',
  'marfa',
  'activated',
  'charcoal',
  'woke',
  'neutra',
  'yolo',
  'lomo',
  'aesthetic',
  'cold-pressed',
  'schlitz',
  'tumblr',
  'listicle',
  'glossier',
  'thundercats',
  'fashion',
  'axe',
  'pabst',
  'bespoke',
  'you',
  'probably',
  "haven't",
  'heard',
  'of',
  'them',
  'whatever',
  'forage',
  'crucifix',
  'chicharrones',
  'seitan',
  'live-edge',
  'venmo',
  'mustache',
  'skateboard',
  'narwhal',
  'af',
  'kogi',
  'master',
  'cleanse',
  'letterpress',
  'hell',
  'plaid',
  'cred',
  'brunch',
  'roof',
  'party',
  'tattooed',
  'banjo',
  'raw',
  'denim',
  'mumblecore',
  'dreamcatcher',
  'cardigan',
  'pinterest',
  'banh',
  'mi',
  'fanny',
  'pack',
  'tofu',
  'paleo',
  'slow-carb',
  'shabby',
  'chic',
  'tacos',
  'artisan',
  'normcore',
  'irony',
  'ugh',
  'put',
  'a',
  'bird',
  'on',
  'it',
  'stumptown',
  'hella',
  'deep',
  'street',
  'art',
  'ennui',
  'pork',
  'belly',
  'try-hard',
  'brooklyn',
  'chicharrones',
  'occupy',
  'vaporware',
  'fixie',
  'gluten-free',
  'vexillologist',
  'flannel',
  'coloring',
  'book',
  'jianbing',
  'lyft',
  'tbh',
  'typewriter',
  'gochujang',
  'echo',
  'park',
  'pok',
  'pok',
  'lo-fi',
  'meditation',
  'scenester',
  'biodiesel',
  'asymmetrical',
  'keffiyeh',
  'polaroid',
  'taiyaki',
  'meggings',
  'microdosing',
  'pour-over',
  'hashtag',
  'mustache',
  'wolf',
  'locavore',
  'readymade',
  'you',
  'swag',
  'knausgaard',
  'pickled',
  'bitters',
  'unicorn',
  'blue',
  'bottle',
  'cray',
  'direct',
  'trade',
  'everyday',
  'carry',
  '8-bit',
  'poke',
  'pickled',
  'single-origin',
  'coffee',
  'blog',
  'edison',
  'bulb',
  'la',
  'croix',
  'la',
  'health',
  'goth',
  'farm-to-table',
  'heirloom',
  'vegan',
  'vinyl',
  'affogato',
  'food',
  'truck',
  'selfies',
  'chambray',
  'quinoa',
  'freegan',
  'portland',
  'wolf',
  'xoxo',
  'taiyaki',
  'cloud',
  'bread',
  'salvia',
  'fam',
  'literally',
  'synth',
  'vice',
  'messenger',
  'bag',
  'kitsch',
  'fingerstache',
  'drinking',
  'vinegar',
  'air',
  'plant',
  'semiotics',
  'offal',
  'shoreditch',
  'farm-to-table',
  'lumbersexual',
  'viral',
  'cardigan',
  'next',
  'level',
  'kale',
  'chips',
  'waistcoat',
  'tumeric',
  'umami',
  'gentrify',
  'gluten-free',
  'godard',
  'banh',
  'pop-up',
  'artisan',
  'pbr&b',
  'cronut',
  'sartorial',
  'keytar',
  'plaid',
  'palo',
  'santo',
  'butcher',
  'roof',
  'mlkshk',
  'four',
  'dollar',
  'toast',
  'wayfarers',
  'tote',
  'cold-pressed',
  'poke',
  'drinking',
  'hoodie',
  'organic',
  'prism',
  'pug',
  'raclette',
  'fashion',
  'celiac',
  'succulents',
  'pork',
  'kombucha',
  'chillwave',
  'urbanism',
  'coldwave',
  'vaporwave',
  'vape',
  'hoverboard'
];

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 * @returns {Array}
 */
function shuffleArray(a) {
  let j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}

/**
 * Returns random integer index between minLength and maxLength
 * @param {number} maxLength
 * @param {number} minLength
 * @returns {number}
 */
const getRandomIndex = (maxLength, minLength = 0) => {
  const randNum = Math.floor(Math.random() * maxLength);
  return randNum + minLength >= maxLength ? randNum : randNum + minLength;
};

/**
 * Transforms first char to uppercase
 * @param {string} str
 * @returns {string}
 */
const firstToUpper = str => str[0].toUpperCase() + str.slice(1);

/**
 * Creates string for title and subtitle
 * @param {string[]} arr Array of words
 * @returns {(strLen : number, min: number) => string}
 */
const createOneLine = arr => (strLen, min = 0) => {
  if (!arr.length) return '';

  const resultStrLength = getRandomIndex(strLen, min);
  let i = 1;
  let result = '';
  while (i <= resultStrLength) {
    result += ` ${arr[getRandomIndex(arr.length)]}`;
    i++;
  }
  result = result.trim();

  return firstToUpper(result);
};

/**
 * Returns random end of the sentence symbol
 * @returns {string}
 */
const getEndSymbol = () => {
  const symbols = ['.', '.', '.', '!', '?', ':)'];
  return symbols[getRandomIndex(symbols.length - 1)];
};

/**
 * Creates string with random number of sentences
 * @param {string[]} arr Array of hipsum words
 * @returns {string}
 */
const createSentences = arr => {
  const numberOfSentences = getRandomIndex(8, 4);
  let endOfSentenceIndexes = {};
  let resString = '';

  for (let i = 0; i < numberOfSentences; i++) {
    endOfSentenceIndexes[getRandomIndex(arr.length - 1, 2)] = true;
  }

  for (let j = 0; j < arr.length; j++) {
    if (endOfSentenceIndexes[j]) {
      arr[j] = arr[j] + getEndSymbol();
      arr[j + 1] = firstToUpper(arr[j + 1]);
    }
    resString = `${resString} ${arr[j]}`;
  }

  return resString.trim() + getEndSymbol();
};

/**
 * Splits Api response into 3 arrays with words
 * @param {string[]} arr Array of words
 * @returns {string[]} arr
 */
const getParagraphs = arr => {
  if (!arr.length) return '';

  const thirdL = Math.floor(arr.length / 3);
  const cutThird = (start, end) =>
    firstToUpper(createSentences(arr.slice(start, end)));

  return [
    cutThird(0, thirdL),
    cutThird(thirdL, thirdL * 2),
    cutThird(thirdL * 2, arr.length)
  ];
};

/**
 * Creates DOM node and adds copy to clipboard event listener to it
 * @param {string} text Element's text content
 * @returns {HTMLElement} node DOM node element
 */
const createDomNode = text => {
  const elem = `
  <div class="text-field">
    <div class="done-overlay">Coppied!</div>
    <button class="copy-button">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
      >
        <path fill="none" d="M0 0h24v24H0V0z" />
        <path
          d="M15 1H4c-1.1 0-2 .9-2 2v13c0 .55.45 1 1 1s1-.45 1-1V4c0-.55.45-1 1-1h10c.55 0 1-.45 1-1s-.45-1-1-1zm.59 4.59l4.83 4.83c.37.37.58.88.58 1.41V21c0 1.1-.9 2-2 2H7.99C6.89 23 6 22.1 6 21l.01-14c0-1.1.89-2 1.99-2h6.17c.53 0 1.04.21 1.42.59zM15 12h4.5L14 6.5V11c0 .55.45 1 1 1z"
        />
      </svg>
    </button>
    <div class="text-area">${text}</div>
  </div>
  `;

  const node = new DOMParser().parseFromString(elem, 'text/html').firstChild;
  const button = node.querySelector('.copy-button');
  const textNode = node.querySelector('.text-area');
  const doneNode = node.querySelector('.done-overlay');

  button.addEventListener('click', () => {
    // add to clipboard functionality
    const selection = window.getSelection();
    const range = document.createRange();
    selection.removeAllRanges();
    range.selectNodeContents(textNode);
    selection.addRange(range);
    document.execCommand('copy');
    selection.removeAllRanges();
    doneNode.classList.add('visible');
  });
  return node;
};

/**
 * Creates Title, Subtitle and 3 paragraphs and renders the to the DOM
 * @param {strig[]} wordArray
 * @param {string} data.text Hipsum text with random words
 */
const renderData = wordArray => {
  const createHeading = createOneLine(wordArray);
  const fragment = document.createDocumentFragment();
  const textsArr = [
    createHeading(6),
    createHeading(12, 6),
    ...getParagraphs(wordArray)
  ];

  textsArr.map(text => fragment.appendChild(createDomNode(text)));

  contentNode.appendChild(fragment);
  preloaderNode.classList.remove('loading');
};

renderData(shuffleArray([...initialArr]));
